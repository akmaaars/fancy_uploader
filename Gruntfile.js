module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserify : {
            options: {
                transform: [
                    [
                        "reactify",
                        {
                            "es6": true
                        }
                    ]
                ]
            },
            app : {
                files : { 'app/assets/javascripts/fancy_uploader/main.js' : ['app/assets/javascripts/fancy_uploader/photos/photos_page.js'] }
            }
        },

        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['node_modules/blueimp-file-upload/js/jquery.fileupload.js'],
                        dest: 'app/assets/javascripts/fancy_uploader/'
                    },
                ]
            }
        }
    });

    grunt.registerTask( 'default', ['copy', 'browserify'])
}

