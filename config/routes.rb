FancyUploader::Engine.routes.draw do

  resources :photos do
    collection { post :sort }
  end

  match '/photos', to: 'photos#create', via: 'patch'

end
