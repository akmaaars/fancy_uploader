class ActiveAdmin::ResourceController
  def assign_photo(object)
    imageable_ids = params[:imageable] || []
    object.photo_ids = imageable_ids
    imageable_ids.each do |imageable_id|
      FancyUploader::Photo.find_by_id(imageable_id).update(title: params[:photo_title][imageable_id])
    end
  end
end