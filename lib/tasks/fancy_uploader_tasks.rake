namespace :fancy_uploader do

  desc "Rebuilds all photos with given style"
  task :rebuild, [:style] => :environment do |t, args|
    FancyUploader::Photo.all.each { |photo| photo.photo_reprocess(args.style) }
  end

end
