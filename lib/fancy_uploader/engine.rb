require 'paperclip'

module FancyUploader
  class Engine < ::Rails::Engine
    isolate_namespace FancyUploader
  end
end
