Rails.application.routes.draw do

  resources :articles

  mount FancyUploader::Engine => "/fancy_uploader"
end
