class AddAttachmentPhotoToPhotos < ActiveRecord::Migration
  def self.up
    change_table :fancy_uploader_photos do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :fancy_uploader_photos, :photo
  end
end
