class AddTitleToPhotos < ActiveRecord::Migration
  def change
    add_column :fancy_uploader_photos, :title, :string
  end
end
