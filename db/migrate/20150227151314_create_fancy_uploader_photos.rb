class CreateFancyUploaderPhotos < ActiveRecord::Migration
  def change
    create_table :fancy_uploader_photos do |t|
      t.integer :position
      t.references :imageable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
