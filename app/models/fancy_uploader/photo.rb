module FancyUploader
  class Photo < ActiveRecord::Base
    belongs_to :imageable, polymorphic: true

    has_attached_file :photo,
                      :styles => Proc.new { |attachment| attachment.instance.styles  }

    validates_attachment_content_type :photo, :content_type => ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']

    def dynamic_photo_url(format)
      @dynamic_style_format = format
      photo.reprocess!(format) unless photo.exists?(format)
      photo.url(format)
    end

    def photo_reprocess(format)
      return if !photo.exists?(format)

      photo.reprocess!(format)
      photo.save
    end

    def styles
      unless @dynamic_style_format.blank?
        parent_styles.merge! thumb
      else
        {}
      end
    end

    def parent_styles
      return thumb if imageable_type.blank?
      klass = imageable_type.classify.safe_constantize
      return thumb if !klass || !klass.respond_to?(:styles)
      klass.styles
    end

    def thumb
      { :thumb => '100x100#' }
    end

  end
end
