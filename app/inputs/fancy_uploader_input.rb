class FancyUploaderInput < Formtastic::Inputs::FileInput

  def to_html
    template.content_tag('div', nil, :id => 'file-uploader',
                         :data => {
                             :existing_photos =>  @object.photos.order(:position).map{
                                 |photo| {id: photo.id, url: photo.dynamic_photo_url(:thumb),
                                          title: photo.title ? photo.title : ''}
                             }
                         })
  end

end