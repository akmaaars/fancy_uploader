var React = require('react');
var FileUploader = require('./FileUploader.jsx');

$(function() {

    var token = $( 'meta[name="csrf-token"]' ).attr( 'content' );

    $.ajaxSetup( {
        beforeSend: function ( xhr ) {
            xhr.setRequestHeader( 'X-CSRF-Token', token );
        }
    });

    $(document).on("drop dragover", function(e) {
        e.preventDefault();
    });

    if ($('#file-uploader').length) {

        var files = $('#file-uploader').data('existing-photos');

        var counter = _create_counter(files.length);

        console.log('DROPZONE FOUND! DROP YOURSELF');
        React.render(
            <FileUploader files={files} counter={counter}/>,
            $('#file-uploader')[0]
        );

    }

    function _create_counter(start) {
        var count = start;
        return function next() {
            return count++;
        }
    }

});

