var React = require('react');
var File = require('./File.jsx');
var FileAdd = require('./FileAdd.jsx');
var FileDone = require('./FileDone.jsx');

var placeholder = document.createElement("div");
placeholder.className = "placeholder";

var FileList = React.createClass({

    dragStart(e) {
        this.dragged = e.currentTarget;
        e.dataTransfer.effectAllowed = 'move';
        //Firefox drag workaround
        e.dataTransfer.setData("text/html", e.currentTarget);
    },

    dragOver(e) {
        e.preventDefault();
        if (typeof(this.dragged) === 'undefined') {
            return;
        }
        var item_class = this.dragged.className;
        if (e.target.className != item_class ) {
            if ($(e.target).closest('.'+item_class).length > 0) {
                this.over = $(e.target).closest('.image-file')[0];
            }
            else {
                return;
            }
        }
        else {
            this.over = e.target;
        }
        this.dragged.style.display = "none";
        var relY = $(window).scrollTop() +  e.clientY - this.over.offsetTop;
        var height = this.over.offsetHeight / 2;
        var parent = this.over.parentNode;

        if (relY > height) {
            this.nodePlacement = "after";
            parent.insertBefore(placeholder, this.over.nextElementSibling);
        }
        else if(relY < height) {
            this.nodePlacement = "before";
            parent.insertBefore(placeholder, this.over);
        }

    },

    dragEnd() {
        this.dragged.style.display = "block";
        this.dragged.parentNode.removeChild(placeholder);

        // Update state
        var data = this.state.data;
        var from = Number(this.dragged.dataset.id);
        var to = Number(this.over.dataset.id);
        if(from < to) to--;
        if(this.nodePlacement == "after") to++;
        data.splice(to, 0, data.splice(from, 1)[0]);
        var newOrder=[];
        data.map(function(file){
            newOrder.push(file.id);
        });
        this.onSort(newOrder);
        this.setState({data: data});
        this.dragged = 'undefined';
    },

    onSort(newOrder) {
        $.ajax({
            type: 'POST',
            url:'/fancy_uploader/photos/sort',
            data: {'photo': newOrder }
        });
    },

    deleteFile(e) {
        e.preventDefault();
        var data = this.state.data;
        $.ajax({
            type: 'DELETE',
            url: e.target.href,
            success: (function(id) {
                data.forEach( function(file, i) {
                    if (file.id == id)
                        delete data[i];
                });
                this.setState(data);
            }).bind(this)
        });
    },

    getInitialState: function() {
        return {data: this.props.files};
    },

    render() {

        var list = this.state.data.map((function(file, index) {

            var sortable_item_props = {
                'data-id': index,
                key: index,
                draggable: 'true',
                onDragEnd: this.dragEnd,
                onDragStart: this.dragStart,
                onDragOver: this.dragOver,
                onSort: this.onSort,
                deleteFile: this.deleteFile,
                className: 'image-file'
            };

            if (file.id == null) {
                return <FileAdd key={index} progress={file.progress} />
            }
            else {
                return <FileDone {...sortable_item_props} id={file.id} url={file.url} title={file.title} />
            }

        }).bind(this));

        return (
            <div className="filelist" >
                {list}
            </div>
        );
    }
});

module.exports = FileList;