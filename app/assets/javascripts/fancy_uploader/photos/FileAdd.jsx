var React = require('react');

var FileAdd = React.createClass({

    _getWidth() {
        return this.props.progress;
    },

    render() {

        var progressbarStyle = {
            width: this._getWidth() + '%'
        };

        return (
            <div className="image-file">
                <div classMame="upload">
                    <div className="progress">
                        <div style={progressbarStyle} className="bar"></div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = FileAdd;