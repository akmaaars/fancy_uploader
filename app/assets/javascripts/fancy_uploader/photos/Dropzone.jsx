var React = require('react');

var DropzoneApp = React.createClass({

    shouldComponentUpdate() {
        return false;
    },

    componentDidMount() {

        $(this.getDOMNode()).on("dragover", function(e) {
            $(this).addClass('drop-dragover');
        });

        $(this.getDOMNode()).on("drop dragleave", function(e) {
            $(this).removeClass('drop-dragover');
        });

        $(this.refs.fileupload.getDOMNode()).fileupload({
            dataType: 'json',
            dropZone: $('#dropzone'),
            add: this.props.onAdd,
            progress: this.props.onProgress,
            done: this.props.onDone,
            fail: function (e, data) {
                alert('Upload failed');
            }
        });
    },

    render() {
        return (
            <div className='dropzone-container' id="dropzone">
                <div className='dropzone-title'>
                    Перетащите файлы сюда или выберите файл:
                </div>
                <input ref="fileupload" data-url="/fancy_uploader/photos" id="photos" multiple="multiple" name="photos[]" type="file" />
            </div>
        );
    }


});

module.exports = DropzoneApp;