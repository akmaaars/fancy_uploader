var React = require('react');
var FileAdd = require('./FileAdd.jsx');
var FileList = require('./FileList.jsx');
var Dropzone = require('./Dropzone.jsx');


var FileUploader = React.createClass({

    getInitialState() {
        var files = this.props.files;
        return {
            files
        };
    },

    _addFile(e, data) {
        data.context = this.props.counter();
        var files = this.state.files;
        files[data.context] = { progress: 0, file: data.files[0], id: null, url: null, context: data.context };
        data.submit();
    },

    _fileProgress(e, data) {
        var files = this.state.files;
        var file = files[data.context];
        file.progress = parseInt(data.loaded / data.total * 100, 10);
        files[data.context] = file;
        this.setState(files);
    },

    _doneUpload(e, data) {
        var files = this.state.files;
        var file = files[data.context];
        file.id = data.result.id;
        file.url = data.result.url;
        files[data.context] = file;
        this.setState(files);
    },

    render() {
        return (
            <div className="fileUploader">
                <FileList files={this.state.files} counter={this.props.counter} />
                <Dropzone onAdd={this._addFile} onProgress={this._fileProgress} onDone={this._doneUpload} />
            </div>
        );
    }
});

module.exports = FileUploader;