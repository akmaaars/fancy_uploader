var React = require('react');
var FileAdd = require('./FileAdd.jsx');
var FileDone = require('./FileDone.jsx');

var File = React.createClass({
    render() {

        var list = [];

        this.props.files.map((function(result) {

            if (result.id == null) {
                list.push(this._getFileAdd(result));
            }
            else {
                list.push(this._getFileDone(result));
            }

        }).bind(this));

        return (
            <div className="image-file">
                {list}
            </div>
        );
    }
});


module.exports = File;
