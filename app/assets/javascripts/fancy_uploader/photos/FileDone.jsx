var React = require('react');

var FileDone = React.createClass({

    mixins: [React.addons.LinkedStateMixin],

    getInitialState() {
        return {
            title: this.props.title
        }
    },

    render() {

        var destroy_url = '/fancy_uploader/photos/'+this.props.id;
        var title_input_name = 'photo_title[' +this.props.id + ']';
        return (
            <div {...this.props}>
                <input value={this.props.id} className = "image-file-input hidden-input" readOnly type="text" name="imageable[]" />
                <div className="handle"></div>
                <div className="img-preview">
                    <img src={this.props.url} />
                </div>
                <input valueLink={this.linkState('title')} className = "image-file-input title-input" placeholder="Title" type="text" name={title_input_name} />
                <div className="destroy-image-link">
                    <a onClick={this.props.deleteFile} href={destroy_url} data-method="delete" data-remote="true" id = "destroy-image-photo.id" />
                </div>
            </div>
        );
    }
});

module.exports = FileDone;