module FancyUploader

  class PhotosController < ApplicationController
    def create
      photo = params[:photos]
      @photo = Photo.new(:photo => photo[0])
      respond_to do |format|
        if @photo.save
          format.html { redirect_to @photo, notice: 'Photo was successfully created.' }
          format.json {
            data = {id: @photo.id, url: @photo.dynamic_photo_url(:thumb)}
            render json: data, status: :created
          }
        else
          format.html { render action: 'new' }
          format.json { render json: @photo.errors, status: :unprocessable_entity }
        end
      end
    end

    def sort
      params[:photo].each_with_index do |id, index|
        Photo.update(id, {position: index+1})
      end
      render nothing: true
    end

    def destroy
      @photo = Photo.find(params[:id])
      respond_to do |format|
        format.js   { render layout: false }
      end
    end

  end
end